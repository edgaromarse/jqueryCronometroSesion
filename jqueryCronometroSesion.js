/**
 * Esta es una extension de jQuery que realiza un conteo regresivo para una sesion
 * Muestra una advertencia despues de cierto tiempo para continuar con la sesion o finaliza la sesi�n por default
 * Para mostrar el mesnaje de alerta depende de la libreria Lobibox
 * v 1.1
 */
// Create closure.
(function( $ ) {
    //Variables privadas
    var idCallbackCronometro = 0
        ,ventanaAlerta = null
        ,cronometroLocal = true
        ,ID_CRONOMETRO_ALERTA = "eose-cronometro-alerta";

    //Estas variables son globales porque se utilizan antes de que se cree el widget
    var fechaInicio = 0
        ,fechaLimite = 0
        ,fechaActual = 0;

    //Entension de jQuery
    $.fn.eoseCronometroSesion = function(options) {
        /*------------------------------------------------------------------------------------------------------*/
        var defaults = {
            fechaInicio: 0      //Debe estar expresada en milisegundos
            ,fechaLimite: 0     //Debe estar expresada en milisegundos
            ,minutosSesion: 1   //Representa minutos
            ,alertaSesion: 30   //Representa segundos antes de finalizar el tiempo de sesion
            ,mensajeAlerta: "Por motivos de seguridad su sesi�n ser� finalizada"
            ,urlCerrarSesion: ""    //URL relativa donde se encuentra el archivo
            ,urlActualizaTiempo: "" //URL relativa donde se encuentra el archivo
            ,nombreDiv: ""
        };

        idCallbackCronometro = 0;
        ventanaAlerta = null;

        //Se mezclan los parametros por default con las opciones
        //que se mandan al llamar la extensi�n
        options.nombreDiv = this.selector;
        options.fechaInicio = Number(options.fechaInicio);
        options.fechaLimite = Number(options.fechaLimite);
        options = $.extend(true, {}, defaults, options);
        /*------------------------------------------------------------------------------------------------------*/

        /*------------------------------------------------------------------------------------------------------*/
        /**
         * Valida si existe el tag donde se desea crear el widget
         * @param obj
         * @returns {boolean}
         */
        var existeTag = function (obj) {
            return !!obj.length;
        };

        /**
         * Valida si se crea o se detiene/elimina el cronometro
         */
        var eoseInicializaWidget = function () {
            if (idCallbackCronometro == 0) {
                //Aun NO se declara la callback de 1 segundo

                eoseReseteaFechas(false);

                //Inicializa el cronometro
                eoseContinuaCronometro();
            } else {
                //Ya existe la callback de 1 segundo entonces se detiene el cronometro
                //y se cierra la sesion
                clearTimeout(idCallbackCronometro);
                $(options.nombreDiv).html("00:00");

                //Eliminamos el valor inicial guardado
                localStorage.removeItem("eose.fecha.inicio");
                localStorage.removeItem("eose.fecha.limite");
                idCallbackCronometro = 0;

                //Se elimina la ventana de ayuda
                if (ventanaAlerta != null) {
                    ventanaAlerta.destroy();
                }
                ventanaAlerta = null;

                //Elimina los datos del widget
                options.fechaInicio = 0;
                options.fechaLimite = 0;

                //Llama la url para cerrar sesion y asi redireccionar al index del login
                window.location.href = options.urlCerrarSesion;
            }
        };

        /**
         * Continua con el cronometro y cada segundo va validando el tiempo restante de la sesion
         */
        var eoseContinuaCronometro = function () {
            //Como la fecha puede variar con la fecha del cliente.
            //Esta funcion se llama al iniciar el componente y despues cada segundo, por lo que a 
            //la fecha inicial del servidor se le va sumando 1 segundo
            fechaActual = new Date(fechaActual + 1000).getTime();

            //Como es un cronometro en cuenta regresiva se resta la fecha limite con la actual
            //var diff=new Date(actual-fechaInicio);
            var diff = new Date(fechaLimite - fechaActual);

            //Se muestra el cronometro en formato "hh:mm:ss"
            var formatoTiempo =
                eoseFormatoTiempo(diff.getUTCHours()) + ":" +
                eoseFormatoTiempo(diff.getUTCMinutes()) + ":" +
                eoseFormatoTiempo(diff.getUTCSeconds());
            $(options.nombreDiv).html( eoseFormatoTiempoVisualiza(formatoTiempo) );

            //Valida que el tiempo de sesion no exceda una hora
            if (diff.getUTCHours() >= 1) {
                //Como puede estar activo el evento o no
                idCallbackCronometro == -1;
                eoseInicializaWidget();
                return false;
            }

            //Valida el tiempo de la sesion
            if (
                diff.getUTCHours() <= 0 &&
                diff.getUTCMinutes() <= 0 &&
                diff.getUTCSeconds() <= 0
            ) {
                //Si llega a cero, se detien el cronometro
                eoseInicializaWidget();
            } else if (
                diff.getUTCHours() <= 0 &&
                diff.getUTCMinutes() <= 0 &&
                diff.getUTCSeconds() <= options.alertaSesion
            ) {
                //Si faltan 30 segundos para llegar a cero muestra un alert
                if (ventanaAlerta == null) {
                    ventanaAlerta = Lobibox.confirm({
                        title: "Su sesion va a finalizar"
                        ,msg: options.mensajeAlerta +
                            " en <label id='"+ID_CRONOMETRO_ALERTA+"'>"+options.alertaSesion+"</label>"
                        ,closeButton: false
                        ,closeOnEsc: false
                        ,buttons: {
                            yes: {
                                text: 'Continuar'
                            }
                            ,no: {
                                text: 'Finalizar'
                            }
                        }
                        ,callback: function ($this, type, ev) {
                            if (type == 'yes') {
                                eoseRestableceTiempo();
                            } else {
                                //Si selecciono "Finalizar" entonces se detiene el cronometro
                                eoseInicializaWidget();
                            }
                        }
                    });
                }
                idCallbackCronometro = setTimeout(function () {
                    eoseContinuaCronometro()
                }, 1000);
            } else {
                // Indicamos que se ejecute esta funci�n nuevamente dentro de 1 segundo
                idCallbackCronometro = setTimeout(function () {
                    eoseContinuaCronometro()
                }, 1000);
            }
        };

        /**
         * Da formato a la hora, minuto o segundo
         * que pone un 0 delante de un valor si es necesario
         * @param piTiempo {number}
         * @returns {string}
         */
        var eoseFormatoTiempo = function (piTiempo) {
            return (piTiempo < 10) ? "0" + piTiempo : +piTiempo;
        };

        var eoseFormatoTiempoVisualiza = function (psTiempo) {
            var hora = psTiempo.substring(0, 2);
            if(hora == '00') {
                psTiempo = psTiempo.substring(3);
            }

            //Si la hora esta en cero entonces toma los minutos
            var minuto = psTiempo.substring(0, 2);
            if(minuto == '00') {
                psTiempo = psTiempo.substring(3);
            }

            return psTiempo;
        };

        /**
         * Restablece las fechas de inicio y limite
         */
        var eoseRestableceTiempo = function () {
            eoseReseteaFechas(true);

            ventanaAlerta = null;
            $(options.nombreDiv).html("00:00");
        };

        /**
         * Restablece las fechas dependiendo si se enviaron al crear el widget
         * @param pbReseteaFechaInicio
         */
        var eoseReseteaFechas = function (pbReseteaFechaInicio) {
            //Se inicializa la fecha inicial
            fechaInicio = (cronometroLocal || pbReseteaFechaInicio) ? fechaActual : options.fechaInicio;

            //Se inicializa la fecha actual
            fechaActual = (pbReseteaFechaInicio) ? fechaActual : options.fechaInicio;

            //Se inicializa la fecha final que contiene los minutos que dura la sesion
            if(pbReseteaFechaInicio){
                fechaLimite = new Date(fechaActual);
                fechaLimite.setTime(fechaLimite.getTime() + options.minutosSesion * 60000);
                fechaLimite = fechaLimite.getTime();
            } else {
                fechaLimite = options.fechaLimite;
            }
            
            if(cronometroLocal) {
                //Guardamos el valor inicial en la base de datos del navegador
                localStorage.setItem("eose.fecha.inicio", fechaInicio);
                localStorage.setItem("eose.fecha.limite", fechaLimite);
            }

            if(pbReseteaFechaInicio) {
                //Llama la url para actualizar las fechas de la sesion
                //Ya no se toma la fecha del servidor si no del ordenador, hasta que actualice la pagina
                $.post(
                    options.urlActualizaTiempo
                    , function () {
                        console.log("Se actualizo el tiempo de la sesion");
                    }
                );
            }
        };
        /*------------------------------------------------------------------------------------------------------*/

        /*------------------------------------------------------------------------------------------------------*/
        //Valida que el div donde se va a mostrar el cronometro exista
        if (!existeTag(this)) {
            console.error('No existe el tag ' + this.selector);
            return false;
        }

        //Valida que las fechas sean fechas validas en el formato dd/mm/aaa hh:mm:ss
        if(options.fechaInicio > 0 && options.fechaLimite > 0){
            cronometroLocal = false;
        }

        //Valida que la fecha limite sea mayor que la inicial
        if(options.fechaInicio > options.fechaLimite){
            console.error('La fecha limite es menor que la fecha inicial');
            return false;
        }

        //valida url
        if(options.urlCerrarSesion == ''){
            console.error('Debe especificar la url relativa para cerrar sesion');
            return false;
        }
        if(options.urlActualizaTiempo == ''){
            console.error('Debe especificar la url relativa para actualizar los tiempos de la sesion');
            return false;
        }
        /*------------------------------------------------------------------------------------------------------*/

        /*------------------------------------------------------------------------------------------------------*/
        eoseEventoCronometro(options.nombreDiv, ID_CRONOMETRO_ALERTA);
        /*------------------------------------------------------------------------------------------------------*/

        /*------------------------------------------------------------------------------------------------------*/
        //Al actualizar la pagina valida si existen datos (base de datos del navegador)
        //de la sesion para continuar el cronometro
        if(localStorage.getItem("eose.fecha.inicio") != null && cronometroLocal) {
            fechaInicio=localStorage.getItem("eose.fecha.inicio");
            fechaLimite=localStorage.getItem("eose.fecha.limite");

            eoseContinuaCronometro();
        } else {
            //Inicializa el widget
            eoseInicializaWidget(this, options);
        }
        /*------------------------------------------------------------------------------------------------------*/

        return this;
    };

    /**
     * Inicializa el evento para detectar cada que se cambie el cronometro
     * @param psSelector {string}
     * @param psIdDuplicado
     */
    function eoseEventoCronometro(psSelector, psIdDuplicado){
        $(psSelector).on("DOMSubtreeModified", function (e) {
            if(document.getElementById(psIdDuplicado) !== null) {
                var texto = $(psSelector).html();
                $("#"+psIdDuplicado).html( texto );
            }
        });
    }

    
// End of closure.
})( jQuery );
