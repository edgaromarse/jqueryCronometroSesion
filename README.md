Cronometro jQuery para sesion PHP
=================================
 
Acerca de
----------------
 
Es un componente de jQuery que muestra un cronometro en cuenta regresiva para
* Controlar sesión en PHP
* Controlar tiempo (solo cronometro)

Dependencias
----------------
* [jQuery](https://jquery.com "jQuery").
* [Lobibox](http://lobianijs.com/site/lobibox "Lobibox").
 
Implementación
----------------
Para inicializar el componente para sesiones de PHP
```[javascript]
$(document).ready(function () {
    $("#etiqueta").eoseCronometroSesion({
        fechaInicio: 1498489100000	//milisegundos
        ,fechaLimite: 1498489100000	//milisegundos
        ,minutosSesion: 5	//minutos que durará la sesion
        ,urlCerrarSesion: "modSesionCerrar.php"
        ,urlActualizaTiempo: "modTiempoSesionActualizar.php"
        //Los siguientes campos son opcionales
        ,mensajeAlerta: "Por motivos de seguridad su sesión será finalizada"
        ,alertaSesion: 30	//segundos restantes en los que aparecerá un mensaje de alerta
    });
});
```

Para inicializar el componente solo como cronometro
```[javascript]
$(document).ready(function () {
    $("#etiqueta").eoseCronometroSesion({
        fechaInicio: 1498489100000	//milisegundos
        ,fechaLimite: 1498489100000	//milisegundos
        ,minutosSesion: 5	//minutos que durará el cronometro
        ,cronometroLocal: true
    });
});
```

Ejemplo
----------------
Directamente poniendo las fechas
```[javascript]
<label id="etiqueta"></label>

<script type="text/javascript">
    $(document).ready(function () {
        $("#etiqueta").eoseCronometroSesion({
            fechaInicio: 1498489100000  //milisegundos
            ,fechaLimite: 1498489400000 //milisegundos
            ,minutosSesion: 5   //minutos que durará la sesion
            ,urlCerrarSesion: "modSesionCerrar.php"
            ,urlActualizaTiempo: "modTiempoSesionActualizar.php"
        });
    });
</script>
```